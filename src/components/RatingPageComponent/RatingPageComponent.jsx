import HeaderComponent from "../common/HeaderComponent/HeaderComponent";
import { RATING_DATA } from "../../constants/constants";
import "./RatingPageComponent.scss";
import { useEffect, useState } from "react";

export default function RatingPageComponent() {
  const [ratingData, setRatingData] = useState([]);

  useEffect(() => {
    fetch("http://localhost:3001/rating")
      .then((res) => res.json())
      .then((res) => setRatingData(res.rating));
  }, []);

  return (
    <>
      <HeaderComponent currentActiveTabNum={1}></HeaderComponent>
      <section className={"rating-section"}>
        <h1 className={"rating-section__headline"}>Рейтинг игроков</h1>
        <table className={"rating-section__table"}>
          <thead>
            <tr>
              <th className={"table__table-head-cell"}>ФИО</th>
              <th className={"table__table-head-cell"}>Всего игр</th>
              <th className={"table__table-head-cell"}>Победы</th>
              <th className={"table__table-head-cell"}>Проигрыши</th>
              <th className={"table__table-head-cell"}>Процент побед</th>
            </tr>
          </thead>
          <tbody>
            {ratingData.map((record) => {
              return (
                <tr key={record.id} className={"table__table-row"}>
                  <td className={"table__table-data-cell"}>{record.name}</td>
                  <td className={"table__table-data-cell"}>
                    {record.totalAmountOfGames}
                  </td>
                  <td className={"table__table-data-cell table__games-won"}>
                    {record.gamesWonAmount}
                  </td>
                  <td className={"table__table-data-cell table__games-lost"}>
                    {record.gamesLostAmount}
                  </td>
                  <td className={"table__table-data-cell"}>
                    {record.winsPercentage}%
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
